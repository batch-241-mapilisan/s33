'https://jsonplaceholder.typicode.com/todos'

const fetchData = async() => {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos')
    const data = await response.json()
    const toDoLists = data.map(todo => todo.title)
    console.log(toDoLists)
}
fetchData()

const fetchSingleData = async() => {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/1')
    const data = await response.json()
    console.log(data)
    console.log(`
        Title: ${data.title}
        Status: ${data.completed}
    `)
}
fetchSingleData()

const createPost = async() => {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "userId": 1,
            "title": 'New Task',
            "completed": false
        })
    })
    const data = await response.json()
    console.log(data)
}
createPost()

const updatePost = async() => {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/2', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "userId": 22,
            "title": 'Updated Task',
            "completed": true
        })
    })
    const data = await response.json()
    console.log(data)
}
updatePost()

const patchPost = async() => {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/3', {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "completed": true,
            "updated": new Date()
        })
    })
    const data = await response.json()
    console.log(data)
}
patchPost()

const deletePost = async() => {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/3', {
        method: 'DELETE'
    })
    const data = await response.json()
    console.log(data)
}
deletePost()